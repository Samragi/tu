Feature: Add Product to the Basket

Scenario: Verify add product to the basket
Given I am on product details page
When I select the size and quantity
And I add a product to the basket
Then I should see product added to the mini basket

Scenario: Verify remove product from the basket
Given I am on basket page
When I select remove button
Then the product should be removed from the basket

