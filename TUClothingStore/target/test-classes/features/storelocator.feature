Feature: Store Locator

Scenario: Verify store locator with valid postcode
Given I am on store locator page
When I search with the valid postcode
Then I should see the list of nearest stores

Scenario: Verify store locator with invalid postcode
Given I am on store locator page
When I search with the invalid postcode
Then I should see no result found

Scenario: Verify store locator with valid town name
Given I am on store locator page
When I search with the valid town name
Then I should see the list of nearest stores

Scenario: Verify store locator with invalid town name
Given I am on store locator page
When I search with the invalid town name
Then I should see no results found

Scenario: Verify store locator with blank
Given I am on store locator page
When I search with blank
Then I should see error message

Scenario: Verify store locator with valid postcode and click and collect
Given I am on store locator page
When I search with the valid postcode 
And click and collect option
Then I should see the list of nearest stores with click and collect option

Scenario: Verify store locator with valid postcode and all category checkboxes 
Given I am on store locator page
When I search with the valid postcode 
And all category checkboxes
Then I should see the list of nearest stores with valid category


