Feature: Search

Scenario: Verify search with product name

Given I am on homepage
When I search with the valid product name 
And I clcik on search button
Then I should see the related product page

Scenario: Verify search with invalid data
Given I am on homepage
When I search for the invalid product
And I clcik on search button
Then I should see no product found

Scenario: Verify search with blank data
Given I am on homepage
When I search with blank
And I clcik on search button
Then I should see enter valid product message

Scenario: Verify search with product category
Given I am on homepage
When I search with the valid product category 
And I clcik on search button
Then I should see the related product category page

Scenario: Verify search with product code
Given I am on homepage
When I search with the valid product code
And I clcik on search button
Then I should see the related product results
