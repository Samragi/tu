package com.cucumberRunner;

import org.openqa.selenium.WebDriver;

import com.driver.Action;
import com.driver.Get;
import com.pages.HomePage;
import com.pages.SearchResultsPage;
import com.pages.StoreLocatorPage;
import com.pages.StoreLocatorResultPage;

public class BaseClass {


	public static WebDriver driver;
	public static HomePage homepage=new HomePage();
	public static SearchResultsPage searchresultspage=new SearchResultsPage();
	public static Action action=new Action();
    public static Get get=new Get();
    public static StoreLocatorPage storelocatorpage=new StoreLocatorPage();
    public static StoreLocatorResultPage storelocatorresultpage=new StoreLocatorResultPage();
    
    
    
}
