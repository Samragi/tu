package com.driver;

import org.openqa.selenium.By;

import com.cucumberRunner.BaseClass;

public class Action extends BaseClass{


public void clickOnElement(By element) {
	driver.findElement(element).click();
}

public void updateTextBox(By element,String searchTerm) {
	driver.findElement(element).clear();
	driver.findElement(element).sendKeys(searchTerm);  
}

}