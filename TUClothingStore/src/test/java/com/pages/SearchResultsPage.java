package com.pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;

import com.cucumberRunner.BaseClass;

public class SearchResultsPage extends BaseClass{

	public void searchRelatedProductPage() {
		Select sortdropdown=new Select(driver.findElement(By.cssSelector("#sortOptions1")));
		sortdropdown.selectByValue("price-desc");
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/search?sort=price-desc&q=jeans%3AnewArrivals&show=Page#", driver.getCurrentUrl());
	}
	
	public void searchNoProductFound() {
		Assert.assertEquals("Sorry, no results for 'ksrbcnsx'",driver.findElement(By.cssSelector("h1")).getText());   
	}
	
	public void searchValidProductMessage() {
		Assert.assertEquals("Please complete a product search",driver.findElement(By.cssSelector("#search-empty-errors")).getText()); 
	}
	
	public void searchValidProductCategoryPage() {
		driver.findElement(By.cssSelector("a[href='/c/kids/girls-pyjamas-and-nightwear?INITD=GNav-CW-GirlsNightwear'].ln-o-bare-link")).click();
		Assert.assertEquals("Girls Pyjamas & Nightwear | Girls PJs | Tu clothing", driver.getTitle());  
	}
	public void searchRelatedProductResultsPage() {
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/search?text=136003198",driver.getCurrentUrl() );   
	
	}
	
}
