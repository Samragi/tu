package com.pages;

import org.junit.Assert;
import org.openqa.selenium.By;

import com.cucumberRunner.BaseClass;

public class StoreLocatorResultPage extends BaseClass {

	public void listOfNearsetStores() {
		 Assert.assertEquals("Store Locator",driver.findElement(By.cssSelector("h1")).getText());   
	}
	
	public void noResultsFound() {
		Assert.assertEquals("Sorry, no results found for your search. Please check your spelling and make sure you are searching for a town name or postcode.",driver.findElement(By.cssSelector("#globalMessages")).getText());				
	}
	
	public void errorMessage() {
		Assert.assertEquals("Store Locator",driver.findElement(By.cssSelector("h1")).getText()); 
	}
	
	public void clickAndCollectOtpion() {
		Assert.assertEquals("Store Locator",driver.findElement(By.cssSelector("h1")).getText());	 
	}
	
	public void categoryOption() {
		Assert.assertEquals("Store Locator",driver.findElement(By.cssSelector("h1")).getText());  
	}
	
}
