package com.pages;

import org.openqa.selenium.By;

import com.cucumberRunner.BaseClass;

public class StoreLocatorPage extends BaseClass {
	
	public static By STORELOCATORBUTTON=By.cssSelector("#tuStoreFinderForm .ln-c-button");
	public static By STORELOCATORTEXTBOX=By.cssSelector(".ln-c-text-input.ln-u-push-bottom");
	public static By CLICKANDCOLLECT=By.cssSelector("label[for='click']");
	String URL="a[href='/store-finder']";

	
		public void storeLocatorePage() {
			get.openURL(URL);
		}

		public void storeLocatorWithValidPostcode() {
		   action.updateTextBox(STORELOCATORTEXTBOX, "HA9 7DQ");
		   
		}
		
		public void storeLocatorWithInvalidPostcode() {
			action.updateTextBox(STORELOCATORTEXTBOX, "h3vQ"); 
			
		}
		
		public void storeLocatorWithValidTownName() {
			action.updateTextBox(STORELOCATORTEXTBOX, "Crawley"); 
			
		}
		
		public void storeLocatoreWithInvalidTownName() {
			action.updateTextBox(STORELOCATORTEXTBOX, "mutly"); 
			
	    }
		
		public void storeLocatorWithClickAndCollect() {
			action.clickOnElement(CLICKANDCOLLECT);
		}
		
		public void storeLocatorWithAllCategoryCheckboxes() {
			driver.findElement(By.cssSelector("label[for='women']")).click();
			driver.findElement(By.cssSelector("label[for='men']")).click();
			driver.findElement(By.cssSelector("label[for='children']")).click();
		
		}
		
		public void clickOnStoreLocatorButton() {
			action.clickOnElement(STORELOCATORBUTTON); 
		}
	}		
		



