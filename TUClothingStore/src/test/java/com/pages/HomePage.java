package com.pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;

import com.cucumberRunner.BaseClass;

public class HomePage extends BaseClass{

	public static By SEARCHTEXTBOX=By.cssSelector("#search");
	public static By SEARCHBUTTON=By.cssSelector(".searchButton");
	String URL= "https://tuclothing.sainsburys.co.uk/";
	
	public void verifyHomePage() {
     
	   System.setProperty("webdriver.chrome.driver", "/Users/samragi/Desktop/Automation/chromedriver");
	   driver=new ChromeDriver();
	   get.openURL(URL);
	  // Assert.assertEquals("Womens, Mens, Kids & Baby Fashion | Tu clothing", driver.getTitle());
	}
 
	public void searchWithValidProductName() {
		action.updateTextBox(SEARCHTEXTBOX, "jeans");
		
	}
	
	public void searchWithInvalidProduct() {
		action.updateTextBox(SEARCHTEXTBOX, "ksrbcnsx");
	
    }

    public void searchWithBlank() {  
    	 action.updateTextBox(SEARCHTEXTBOX, "");
		
    }
    
    public void searchWithValidProductCategory() {
    	 action.updateTextBox(SEARCHTEXTBOX, "kids");
		
    }

    public void searchWithValidProductCode() {
    	 action.updateTextBox(SEARCHTEXTBOX, "136003198");
    }
    
	public void clickOnSearchButton() {
		action.clickOnElement(SEARCHBUTTON);
	}
}
