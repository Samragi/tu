package com.stepDefination;

import com.cucumberRunner.BaseClass;

import cucumber.api.java.en.Then;

public class SearchResultsPageSD extends BaseClass {

	@Then("^I should see the related product page$")
	public void i_should_see_the_related_product_page() throws Throwable {
		searchresultspage.searchRelatedProductPage();
	}
	
	@Then("^I should see no product found$")
	public void i_should_see_no_product_found() throws Throwable {
		searchresultspage.searchNoProductFound();   
	}
	
	@Then("^I should see enter valid product message$")
	public void i_should_see_enter_valid_product_message() throws Throwable {
		searchresultspage.searchValidProductMessage();
	}
	
	@Then("^I should see the related product category page$")
	public void i_should_see_the_related_product_category_page() throws Throwable {
		searchresultspage.searchValidProductCategoryPage();
	}
	
	@Then("^I should see the related product results$")
	public void i_should_see_the_related_product_results() throws Throwable {
		searchresultspage.searchRelatedProductResultsPage();
	}

}
