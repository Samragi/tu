package com.stepDefination;

import com.cucumberRunner.BaseClass;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;

public class StoreLocatorPageSD extends BaseClass {

	@Given("^I am on store locator page$")
	public void i_am_on_store_locator_page() throws Throwable {	   
		storelocatorpage.storeLocatorePage();
	}

	@When("^I search with valid postcode$")
	public void i_search_with_the_valid_postcode() throws Throwable {
		storelocatorpage.storeLocatorWithValidPostcode();
	}

	@When("^I search with the invalid postcode$")
	public void i_search_with_the_invalid_postcode() throws Throwable {
		storelocatorpage.storeLocatorWithInvalidPostcode();
	}

	@When("^I search with the valid town name$")
	public void i_search_with_the_valid_town_name() throws Throwable {
		storelocatorpage.storeLocatorWithValidTownName();
	}

	@When("^I search with the invalid town name$")
	public void i_search_with_the_invalid_town_name() throws Throwable {
	    storelocatorpage.storeLocatoreWithInvalidTownName();
	}

	@When("^click and collect option$")
	public void click_and_collect_option() throws Throwable {
		storelocatorpage.storeLocatorWithClickAndCollect();
	}

	@When("^all category checkboxes$")
	public void all_category_checkboxes() throws Throwable {
		storelocatorpage.storeLocatorWithAllCategoryCheckboxes();
	}

	

	
	
	
	
	
	
	
	
	
	
	
	
	
}
