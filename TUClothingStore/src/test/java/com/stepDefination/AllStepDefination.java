package com.stepDefination;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class AllStepDefination {

	public static WebDriver driver;
	
	@Given("^I am on homepage$")
	public void i_am_on_homepage() throws Throwable {
		System.setProperty("webdriver.chrome.driver", "/Users/samragi/Desktop/Automation/chromedriver");
		   driver=new ChromeDriver();
		   driver.get("https://tuclothing.sainsburys.co.uk/");
	       Assert.assertEquals("Womens, Mens, Kids & Baby Fashion | Tu clothing", driver.getTitle());
	  }

	@When("^I search with the valid product name$")
	public void i_search_with_the_valid_product_name() throws Throwable {
		driver.findElement(By.cssSelector("#search")).clear();
		driver.findElement(By.cssSelector("#search")).sendKeys("jeans"); 
		driver.findElement(By.cssSelector(".searchButton")).click();
	}

	@Then("^I should see the related product page$")
	public void i_should_see_the_related_product_page() throws Throwable {
		driver.findElement(By.cssSelector(".searchButton")).click();
		Select sortdropdown=new Select(driver.findElement(By.cssSelector("#sortOptions1")));
		sortdropdown.selectByValue("price-desc");
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/search?sort=price-desc&q=jeans%3AnewArrivals&show=Page#", driver.getCurrentUrl());
	
	}

	@When("^I search for the invalid product$")
	public void i_search_for_the_invalid_product() throws Throwable {
		driver.findElement(By.cssSelector("#search")).clear();
		driver.findElement(By.cssSelector("#search")).sendKeys("ksrbcnsx");  
		driver.findElement(By.cssSelector(".searchButton")).click();
	}

	@Then("^I should see no product found$")
	public void i_should_see_no_product_found() throws Throwable {
		
	    Assert.assertEquals("Sorry, no results for 'ksrbcnsx'",driver.findElement(By.cssSelector("h1")).getText());   
	}

	@When("^I search with blank data$")
	public void i_search_with_blank_data() throws Throwable {
		driver.findElement(By.cssSelector("#search")).sendKeys("");
		driver.findElement(By.cssSelector("#search")).click();  
	}

	@Then("^I should see enter valid product message$")
	public void i_should_see_enter_valid_product_message() throws Throwable {
		Assert.assertEquals("Please complete a product search",driver.findElement(By.cssSelector("#search-empty-errors")).getText()); 
	}

	@When("^I search with the valid product category$")
	public void i_search_with_the_valid_product_category() throws Throwable {
		driver.findElement(By.cssSelector("#search")).clear();
		driver.findElement(By.cssSelector("#search")).sendKeys("kids");  
		driver.findElement(By.cssSelector(".searchButton")).click();
	}

	@Then("^I should see the related product category page$")
	public void i_should_see_the_related_product_category_page() throws Throwable {
		driver.findElement(By.cssSelector("a[href='/c/kids/girls-pyjamas-and-nightwear?INITD=GNav-CW-GirlsNightwear'].ln-o-bare-link")).click();
		Assert.assertEquals("Girls Pyjamas & Nightwear | Girls PJs | Tu clothing", driver.getTitle());  
	}

	@When("^I search with the valid product code$")
	public void i_search_with_the_valid_product_code() throws Throwable {
		driver.findElement(By.cssSelector("#search")).clear();
		driver.findElement(By.cssSelector("#search")).sendKeys("136003198");  
		driver.findElement(By.cssSelector(".searchButton")).click();
		}

	@Then("^I should see the related product results$")
	public void i_should_see_the_related_product_results() throws Throwable {
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/search?text=136003198",driver.getCurrentUrl() );   
	}
	
	@When("^I search with blank$")
	public void i_search_with_blank() throws Throwable {
		driver.findElement(By.cssSelector("#tuStoreFinderForm .ln-c-button")).click();
		Assert.assertEquals("Store Locator",driver.findElement(By.cssSelector("h1")).getText());
	}

	@Given("^I am on store locator page$")
	public void i_am_on_store_locator_page() throws Throwable {	   
		driver.findElement(By.cssSelector(("a[href='/store-finder']"))).click();
	}

	@When("^I search with the valid postcode$")
	public void i_search_with_the_valid_postcode() throws Throwable {
		driver.findElement(By.cssSelector(".ln-c-text-input.ln-u-push-bottom")).sendKeys("HA9 7DQ");
	    driver.findElement(By.cssSelector("#tuStoreFinderForm .ln-c-button")).click();  
	}

	@Then("^I should see the list of nearest stores$")
	public void i_should_see_the_list_of_nearest_stores() throws Throwable {
		Assert.assertEquals("Store Locator",driver.findElement(By.cssSelector("h1")).getText());   
	}

	@When("^I search with the invalid postcode$")
	public void i_search_with_the_invalid_postcode() throws Throwable {
		driver.findElement(By.cssSelector(".ln-c-text-input.ln-u-push-bottom")).clear();
		driver.findElement(By.cssSelector(".ln-c-text-input.ln-u-push-bottom")).sendKeys("h3vQ");   
		driver.findElement(By.cssSelector("#tuStoreFinderForm .ln-c-button")).click();
		}

	@Then("^I should see no result found$")
	public void i_should_see_no_result_found() throws Throwable {
		Assert.assertEquals("Sorry, no results found for your search. Please check your spelling and make sure you are searching for a town name or postcode.",driver.findElement(By.cssSelector("#globalMessages")).getText());			
		
	}

	@When("^I search with the valid town name$")
	public void i_search_with_the_valid_town_name() throws Throwable {
		driver.findElement(By.cssSelector(".ln-c-text-input.ln-u-push-bottom")).clear();
		driver.findElement(By.cssSelector(".ln-c-text-input.ln-u-push-bottom")).sendKeys("Crawley");
	    driver.findElement(By.cssSelector("#tuStoreFinderForm .ln-c-button")).click();
	}

	@When("^I search with the invalid town name$")
	public void i_search_with_the_invalid_town_name() throws Throwable {
		driver.findElement(By.cssSelector(".ln-c-text-input.ln-u-push-bottom")).clear();
		driver.findElement(By.cssSelector(".ln-c-text-input.ln-u-push-bottom")).sendKeys("mutly");
		driver.findElement(By.cssSelector("#tuStoreFinderForm .ln-c-button")).click();    
	}

	@Then("^I should see no results found$")
	public void i_should_see_no_results_found() throws Throwable {
		Assert.assertEquals("Sorry, no results found for your search. Please check your spelling and make sure you are searching for a town name or postcode.",driver.findElement(By.cssSelector("#globalMessages")).getText());			
		 
	}

	@Then("^I should see error message$")
	public void i_should_see_error_message() throws Throwable {
		Assert.assertEquals("Store Locator",driver.findElement(By.cssSelector("h1")).getText()); 
	}

	@When("^click and collect option$")
	public void click_and_collect_option() throws Throwable {
		driver.findElement(By.cssSelector(("label[for='click']"))).click();  
	}

	@Then("^I should see the list of nearest stores with click and collect option$")
	public void i_should_see_the_list_of_nearest_stores_with_click_and_collect_option() throws Throwable {
		Assert.assertEquals("Store Locator",driver.findElement(By.cssSelector("h1")).getText());	 
	}

	@When("^all category checkboxes$")
	public void all_category_checkboxes() throws Throwable {
		driver.findElement(By.cssSelector("label[for='women']")).click();
		driver.findElement(By.cssSelector("label[for='men']")).click();
		driver.findElement(By.cssSelector("label[for='children']")).click();
		driver.findElement(By.cssSelector(("label[for='click']"))).click();  
	}

	@Then("^I should see the list of nearest stores with valid category$")
	public void i_should_see_the_list_of_nearest_stores_with_valid_category() throws Throwable {
		Assert.assertEquals("Store Locator",driver.findElement(By.cssSelector("h1")).getText());  
	}

@Given("^I am on product details page$")
public void i_am_on_product_details_page() throws Throwable {
   // driver.findElement(By.cssSelector("#page")).click();
	Assert.assertEquals("https://tuclothing.sainsburys.co.uk/p/Mini-Me-Navy-Suit-Jacket-%283-14-Years%29/134914721-Navy?searchTerm=134914721:newArrivals&searchProduct=",driver.getCurrentUrl());
}

@When("^I select the size and quantity$")
public void i_select_the_size_and_quantity() throws Throwable {
	Select Size=new Select(driver.findElement(By.cssSelector("#select-size")));
	   Size.selectByValue("134914748"); 
}

@When("^I add a product to the basket$")
public void i_add_a_product_to_the_basket() throws Throwable {
	try {
		 Thread.sleep(3000);
	    } catch (InterruptedException e) {
		 e.printStackTrace();
	   }
	   driver.findElement(By.cssSelector("button[title='Add a basket']")).click();
	  
 }

@Then("^I should see product added to the mini basket$")
public void i_should_see_product_added_to_the_mini_basket() throws Throwable {
	 Assert.assertEquals("https://tuclothing.sainsburys.co.uk/p/Mini-Me-Navy-Suit-Jacket-%283-14-Years%29/134914721-Navy?searchTerm=134914721:newArrivals&searchProduct=", driver.getCurrentUrl());  
}

@Given("^I am on basket page$")
public void i_am_on_basket_page() throws Throwable {
	Assert.assertEquals("https://tuclothing.sainsburys.co.uk/cart",driver.getCurrentUrl());
	//driver.findElement(By.cssSelector("a.doCheckoutBut.tuButton.ln-c-button")).click();   
}

@When("^I select remove button$")
public void i_select_remove_button() throws Throwable {
	driver.findElement(By.cssSelector("#RemoveProduct_0")).click();  
	Assert.assertEquals("https://tuclothing.sainsburys.co.uk/cart",driver.getCurrentUrl());
}
}

