package com.stepDefination;



import org.junit.Assert;
import org.openqa.selenium.chrome.ChromeDriver;

import com.cucumberRunner.BaseClass;

import cucumber.api.java.After;
import cucumber.api.java.Before;

public class Hooks extends BaseClass{
	String URL= "https://tuclothing.sainsburys.co.uk/";
	
	@Before
	public void start() {
		 System.setProperty("webdriver.chrome.driver", "/TUClothingStore/src/test/resources/Drivers");
		   driver=new ChromeDriver();
		   get.openURL(URL);
		   Assert.assertEquals("Womens, Mens, Kids & Baby Fashion | Tu clothing", driver.getTitle());
		}
	
	
	@After
	public void close() {
		driver.close();
	}
	
	
	
}
