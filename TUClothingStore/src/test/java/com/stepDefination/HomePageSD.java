package com.stepDefination;
import com.cucumberRunner.BaseClass;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;

public class HomePageSD extends BaseClass {

	@Given("^I am on homepage$")
	public void i_am_on_homepage() throws Throwable{
		homepage.verifyHomePage();
	  }
	
	@When("^I search with the valid product name$")
	public void i_search_with_the_valid_product_name() throws Throwable {
		homepage.searchWithValidProductName();
	}
	
	@When("^I search for the invalid product$")
	public void i_search_for_the_invalid_product() throws Throwable {
		homepage.searchWithInvalidProduct();
	}
	
	@When("^I search with blank data$")
	public void i_search_with_blank_data() throws Throwable {
		homepage.searchWithBlank();
	}
	

	@When("^I search with the valid product category$")
	public void i_search_with_the_valid_product_category() throws Throwable {
		homepage.searchWithValidProductCategory();
	}

	@When("^I search with the valid product code$")
	public void i_search_with_the_valid_product_code() throws Throwable {
		homepage.searchWithValidProductCode();
		}

	@When("^ I click on search button$")
	public void i_click_on_search_button() throws Throwable {
		homepage.clickOnSearchButton();
	}
}
