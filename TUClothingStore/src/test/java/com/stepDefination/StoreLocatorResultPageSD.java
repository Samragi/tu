package com.stepDefination;

import com.cucumberRunner.BaseClass;

import cucumber.api.java.en.Then;

public class StoreLocatorResultPageSD extends BaseClass {

	@Then("^I should see the list of nearest stores$")
	public void i_should_see_the_list_of_nearest_stores() throws Throwable {
		storelocatorresultpage.listOfNearsetStores();
	}
			
	@Then("^I should see no result found$")
	public void i_should_see_no_result_found() throws Throwable {
		storelocatorresultpage.noResultsFound();	
	}

	@Then("^I should see error message$")
	public void i_should_see_error_message() throws Throwable {
		storelocatorresultpage.errorMessage();
	}
	
	@Then("^I should see the list of nearest stores with click and collect option$")
	public void i_should_see_the_list_of_nearest_stores_with_click_and_collect_option() throws Throwable {
		storelocatorresultpage.clickAndCollectOtpion();
	}
	
	@Then("^I should see the list of nearest stores with valid category$")
	public void i_should_see_the_list_of_nearest_stores_with_valid_category() throws Throwable {
		storelocatorresultpage.categoryOption();
	}
}
