package com.stepDefination;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;

import com.cucumberRunner.BaseClass;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class ProductDetailPageSD extends BaseClass{

	@Given("^I am on product details page$")
	public void i_am_on_product_details_page() throws Throwable {
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/p/Mini-Me-Navy-Suit-Jacket-%283-14-Years%29/134914721-Navy?searchTerm=134914721:newArrivals&searchProduct=",driver.getCurrentUrl());
	}

	@When("^I select the size and quantity$")
	public void i_select_the_size_and_quantity() throws Throwable {
		Select Size=new Select(driver.findElement(By.cssSelector("#select-size")));
		   Size.selectByValue("134914748"); 
	}

	@When("^I add a product to the basket$")
	public void i_add_a_product_to_the_basket() throws Throwable {
		try {
			 Thread.sleep(3000);
		    } catch (InterruptedException e) {
			 e.printStackTrace();
		   }
		   driver.findElement(By.cssSelector("button[title='Add a basket']")).click();
		  
	 }

	@Then("^I should see product added to the mini basket$")
	public void i_should_see_product_added_to_the_mini_basket() throws Throwable {
		 Assert.assertEquals("https://tuclothing.sainsburys.co.uk/p/Mini-Me-Navy-Suit-Jacket-%283-14-Years%29/134914721-Navy?searchTerm=134914721:newArrivals&searchProduct=", driver.getCurrentUrl());  
	}

}
